
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
	//TESTING PURPOSE ONLY:
    return cookieValue;
}

function deleteCookie(name) {
    document.cookie = encodeURIComponent(name) + "=deleted; expires=" + new Date(0).toUTCString();
}


var userID;

$(document).ready(function() {	


    if (getCookie("access_token") == null || getCookie("access_token") == "") {
        $("#signin").show();
        $("#acksignin").text("").hide();
    } else {
        $.ajax({
            type: "get",
            url: "http://cs3213.herokuapp.com/users/current.json?access_token=" + getCookie("access_token"),
            cache: false,
            error: function(jqXHR, textStatus, error) {
                console.log("Could not get current user data - " + textStatus + ": " + error);
            },
            success: function(data, textStatus, jqXHR) {
                $("#signin").hide();
                $("#acksignin").html(data.email + '<i class="icon-circle-arrow-down"></i>').show();
            }
        });
    }
	
    var Movie = new CompassModel({ url: "http://cs3213.herokuapp.com/movies"});
	
	var CreateMovie = new CompassModel({ url: "http://cs3213.herokuapp.com/movies"});
	
	var UpdateMovie = new CompassModel({ url: "http://cs3213.herokuapp.com/movies"});
	
	var DeleteMovie = new CompassModel({ url: "http://cs3213.herokuapp.com/movies"});
	
	var Review = new CompassModel({ url: "http://cs3213.herokuapp.com/movies"});
	
	var MoviesCollection = new CompassCollection({ model: Movie});
	
	var ReviewsCollection = new CompassCollection({ model: Review});
	
	var ListMoviesView = new CompassView({ template: $("#list-movies-template")});
	
	var SingleMovieView = new CompassView({ template: $("#single-movie-template")});
	
	var CreateMovieView = new CompassView({
        template: $("#create-movie-template"),
        events: {
            'click #submit': function createMovieEvent() {
                var token = getCookie("access_token");
                if (token == null || token == "") {
                    alert("You need to log in first.");
                } else {
                    var title = $.trim($("#movie_title").val());
                    var summary = $.trim($("#movie_summary").val());
                    var img = $.trim($("#movie_img").val());
                    if (title == "" || summary == "" || img == "") {
                        alert("You must provide a title, summary and an image.");
                    } else {
                        $("#submit").attr('disabled', 'disabled');
                        CreateMovie.set({
                            'access_token': token,
                            'movie[title]': title,
                            'movie[summary]': summary,
                            'movie[img]': document.getElementById('movie_img').files[0]
                        });
						console.log(CreateMovie);
                        CreateMovie.save({
							suffix: ".json",
                            success: function(data) {
								window.location.hash = "#movies/" + data.id;
                            },
                            error: function(error) {
                                console.log(error);
                            }
                        });
                    }
                }
            }
        }
    });
	
	var UpdateMovieView = new CompassView({
        template: $("#update-movie-template"),
        events: {
            'click #updateMovieBtn': function createMovieEvent(bindedModel) {
                var token = getCookie("access_token");
                if (token == null || token == "") {
                    alert("You need to log in first.");
                } else {
                    var title = $.trim($("#movie_title").val());
                    var summary = $.trim($("#movie_summary").val());
                    if (title == "" || summary == "") {
                        alert("You must provide a title and summary.");
                    } else {
                        $("#updateMovieBtn").attr('disabled', 'disabled');
                        var newData = {
                            'access_token': token,
                            'movie[title]': title,
                            'movie[summary]': summary,
                        };
                        if ($.trim($("#movie_img").val())) {
                            newData['movie[img]'] = document.getElementById('movie_img').files[0];
                        }
                        bindedModel.set(newData);
                        bindedModel.sync({
                            suffix: "/"+bindedModel.obj.id + ".json",
                            success: function() {
                                window.location.href = "#movies/" + bindedModel.obj.id;
                            },
                            error: function(error) {
                                console.log(error);
                                alert(error);
                            }
                        });
                    }
                }
            }
        },
    });
	
    
	
	ListMoviesView.bindElement("#list_movies");
	
	ListMoviesView.renderMoviesPage = function(pageNumber) {
		$(this.element).empty();
		this.render();
        if (pageNumber == 1) {
            $("#prevLink").hide();
            $("#nextLink").attr("href", "#page/" + (pageNumber + 1)).show();
        } else {
            $("#prevLink").attr("href", "#page/" + (pageNumber - 1)).show()
            $("#nextLink").attr("href", "#page/" + (pageNumber + 1)).show();
        }
    };
	
	SingleMovieView.bindElement("#list_movies");
	
	CreateMovieView.bindElement("#list_movies");
	
	UpdateMovieView.bindElement("#list_movies");
   	
	var AppRouter = new CompassRouter({
        routes: {
            "": "index",
            "page/:page": "movies_pagination",
            "movies/:id": "view_movie",
            "movie/delete/:id": "delete_movie",
            "movie/update/:id": "update_movie",
            "new_movie": "new_movie",
            "movie/:mid/review/delete/:rid": "delete_review",
            "logout": "logout",
            "review/create/:mid": "create_review"
        },
        handlers: {
            index: function() {
				console.log("INDEX");
                // load first page of movies
                MoviesCollection.get({
					suffix: ".json",
                    success: function(data) {
                        ListMoviesView.bindModel({
                            model: data
                        });
                        ListMoviesView.renderMoviesPage(1);
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            },
            movies_pagination: function(params) {
                console.log("MOVIE PAGE");
				var page = parseInt(params.page);
                MoviesCollection.get({
					suffix: ".json",
                    additionalParams: {
                        page: page
                    },
                    success: function(data) {
						console.log("page: "+page);
                        ListMoviesView.bindModel({
                            model: data
                        });
                        ListMoviesView.renderMoviesPage(parseInt(page));
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            },
			view_movie: function(params) {
				console.log("VIEW MOVIE");
				var token = getCookie("access_token");
				console.log("Token: "+token);
				if (token!=null) {
					$.ajax({
						type: "get",
						url: "http://cs3213.herokuapp.com/users/current.json?access_token=" + token,
						success: function(data, textStatus, jqXHR) {
							userID = data.id;
							Movie.get({
								suffix: "/"+params.id + ".json",
								success: function(data) {
									console.log(data);
									ReviewsCollection.get({
										customUrl: "http://cs3213.herokuapp.com/movies/" + params.id + "/reviews.json",
										success: function(allReviews) {
											console.log(allReviews);
											Movie.set({
												reviews: allReviews
											});
											SingleMovieView.bindModel({
												model: Movie
											});
											$("#list_movies").empty();
											SingleMovieView.render();
											$("abbr.timeago").timeago();
										},
										error: function(error) {
											console.log("Review error");
											console.log(error);
										}
									});
								},
								error: function(errorMsg) {
									console.log("Movie error");
									console.log(errorMsg);
								}
							});
						}
					});
				} else {
				console.log("I don't get token");
							Movie.get({
								suffix: "/"+params.id + ".json",
								success: function(data) {
									console.log(data);
									ReviewsCollection.get({
										customUrl: "http://cs3213.herokuapp.com/movies/" + params.id + "/reviews.json",
										success: function(allReviews) {
											console.log(allReviews);
											Movie.set({
												reviews: allReviews
											});
											SingleMovieView.bindModel({
												model: Movie
											});
											$("#list_movies").empty();
											SingleMovieView.render();
											$("abbr.timeago").timeago();
										},
										error: function(error) {
											console.log("Review error");
											console.log(error);
										}
									});
								},
								error: function(errorMsg) {
									console.log("Movie error");
									console.log(errorMsg);
								}
							});
				}


            },
			new_movie: function() {
				console.log("NEW MOVIE");
                $("#list_movies").empty();
                CreateMovieView.render();
				console.log("RENDERING SUCCESSFUL");
                CreateMovieView.delegateEvents();
            },
			update_movie: function(params) {
				console.log("UPDATE MOVIE");
                var movieid = params.id;
                UpdateMovie.get({
                    suffix: "/"+movieid + ".json",
                    success: function(data) {
						console.log("retrieval for update successful");
						console.log(data);
                        $("#list_movies").empty();
                        UpdateMovieView.bindModel({ model: UpdateMovie});
                        UpdateMovieView.render();
                        UpdateMovieView.delegateEvents();
                    },
                    error: function(errorMsg) {
                        console.log(errorMsg);
                    }
                });
            },
			delete_movie: function(params) {
                $("#deleteMovieBtn").attr("disabled", "disabled");
                var movieid = params.id;
                var token = getCookie("access_token");
                if (token == null || token == "") {
                    alert("You need to log in first.");
                    window.location.href = "#movies/" + movieid;
                } else {
                    // we want the user id so that we know whether he is authorized to delete the movie
                    $.ajax({
                        type: "get",
                        url: "http://cs3213.herokuapp.com/users/current.json?access_token=" + token,
                        success: function(data, textStatus, jqXHR) {
                            var uid = data.id;
                            DeleteMovie.get({
                                suffix: "/"+movieid + ".json",
                                success: function(data) {
                                    if (uid != DeleteMovie.obj.user.id) {
                                        alert("You are not authorized to delete this movie");
                                        $("#deleteMovieBtn").removeAttr("disabled");
                                    } else {
                                        DeleteMovie.set({
                                            'access_token': token
                                        });
                                        DeleteMovie.destroy({
                                            suffix: "/"+DeleteMovie.obj.id + ".json",
                                            success: function() {
                                                window.location.href = "";
                                            },
                                            error: function(error) {
                                                console.log("error attempting to delete obj: " + error);
                                                alert("error: ", error);
                                                $("#deleteMovieBtn").removeAttr("disabled");
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    });
                }
            },
			logout: function() {
                deleteCookie("access_token");
                window.location.href = "";
            },
			create_review: function(params) {
                var movieid = params.mid;
                var token = getCookie("access_token");
                if (token == null || token == "") {
                    alert("You need to log in first.");
                    return;
                } else {
                    var comment = $.trim($("#review_comment").val());
                    var score = $.trim($("#review_score").val());
                    if (score < 1 || score > 100) {
                        alert("Please enter a score between 1 and 100");
                        window.location.href = "#movies/" + movieid;
                    } else {
                        var data = {
                            'movie_id': movieid,
                            'score': score,
                            'comment': comment,
                            'access_token': token
                        };
                        var url = "http://cs3213.herokuapp.com/movies/" + movieid + "/reviews.json";
                        $.ajax({
                            url: url,
                            type: "POST",
							cache: false,
                            dataType: "json",
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            data: JSON.stringify(data),
                            success: function(result) {
                                window.location.href = "#movies/" + movieid;
                            },
                            error: function(xhr, status, err) {
                                console.log(xhr);
                            }
                        });
                    }
                }
            },
			delete_review: function(params) {
                var movieid = params.mid;
                var reviewid = params.rid;
                var token = getCookie("access_token");
                if (token == null || token == "") {
                    alert("You need to log in first.");
                    window.location.href = "#movies/" + movieid;
                } else {
                    $(".deleteReviewBtn").attr("disabled", "disabled");
                    // Manual delete
                    $.ajax({
                        type: 'delete',
						cache: false,
                        url: 'http://cs3213.herokuapp.com/movies/' + movieid + '/reviews/' + reviewid + '.json',
                        data: {
                            'access_token': token
                        },
						success: function(result) {
                                window.location.href = "#movies/" + movieid;
								console.log("succesfully delete review!");
                            },
                        error: function(jqXHR, textStatus, error) {
                            console.log(textStatus + ": " + error);
                            alert(error);
                            $(".deleteReviewBtn").removeAttr("disabled");
                        }
                    });
                }
            }
		}
	})
	
	

	AppRouter._route();
	CompassHistory.start();
})